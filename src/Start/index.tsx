import { Route, Routes } from "react-router-dom";
import SignUP from "../container/Register";
import Todo from "../container/Todo";
import Login from "../container/Login";

function Start() {
  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<SignUP />} />
      <Route path="/todo" element={<Todo />} />
    </Routes>
  );
}

export default Start;
