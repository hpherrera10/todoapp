import { useEffect, useState } from "react";

import { IconButton } from "@material-ui/core";
import Snackbar from "@mui/material/Snackbar";
import { Close } from "@material-ui/icons";

import { Props, State } from "./types";

function Alert(props: Props) {
  const { message } = props;

  const initialState: State = {
    open: false,
  };
  const [state, setState] = useState(initialState);

  useEffect(() => {
    setState({ ...state, open: message !== "" });
  }, [message]);

  const handleClose = () => setState({ ...state, open: false });

  const action = () => (
    <IconButton
      size="small"
      aria-label="close"
      color="inherit"
      onClick={handleClose}
    >
      <Close fontSize="small" />
    </IconButton>
  );

  return (
    <Snackbar
      open={state.open}
      message={message}
      onClose={() => handleClose}
      action={action()}
    />
  );
}

export default Alert;
