export interface Props {
  message: string;
}

export interface State {
  open: boolean;
}
