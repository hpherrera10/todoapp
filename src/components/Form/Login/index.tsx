import { useState } from "react";
import { Button, Card, Link, TextField, Typography } from "@material-ui/core";

import { Props, State } from "./types";
import useStyles from "./styles";
import { USerServ } from "../../../services";

function LoginForm(props: Props) {
  const { handleSubmit } = props;

  const initialState: State = {
    userEmail: "",
    userPass: "",
    message: "",
  };
  const [state, setState] = useState(initialState);

  const styled = useStyles();

  const handleLogin = () => {
    if (state.userEmail === "" || state.userPass === "") {
      setState({ ...state, message: "Todos los campos son requeridos" });
    } else {
      if (USerServ.validateEmail(state.userEmail)) {
        handleSubmit(state);
      } else {
        setState({
          ...state,
          message: "El campo email debe ser del tipo email",
        });
      }
    }
  };

  const rendertitle = () => (
    <Typography variant="h5" component="h5" color="primary">
      Iniciar Sesión
    </Typography>
  );

  const renderEmail = () => (
    <TextField
      id="outlined-basic"
      label="Email"
      variant="outlined"
      margin="normal"
      fullWidth
      type="email"
      required
      value={state.userEmail}
      onChange={(e) => {
        setState({ ...state, userEmail: e.target.value, message: "" });
      }}
    />
  );

  const renderPassword = () => (
    <TextField
      id="outlined-basic"
      label="Contraseña"
      variant="outlined"
      fullWidth
      type="password"
      margin="normal"
      required
      value={state.userPass}
      onChange={(e) =>
        setState({ ...state, userPass: e.target.value, message: "" })
      }
    />
  );

  const renderMessage = () => (
    <Typography variant="body1" color="secondary">
      {state.message}
    </Typography>
  );

  const renderButton = () => (
    <Button
      variant="contained"
      color="primary"
      onClick={() => handleLogin()}
      fullWidth
      className={styled.button}
    >
      Iniciar Sesión
    </Button>
  );

  const renderLinkSignUp = () => (
    <Typography>
      ¿No tiene una cuenta?{" "}
      <Link href="/signup" variant="body2">
        Regístrese
      </Link>
    </Typography>
  );

  return (
    <Card className={styled.card}>
      {rendertitle()}
      {renderEmail()}
      {renderPassword()}
      {state.message !== "" && renderMessage()}
      {renderButton()}
      {renderLinkSignUp()}
    </Card>
  );
}

export default LoginForm;
