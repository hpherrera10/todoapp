import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      width: "24%",
      alignItems: "center",
      textAlign: "center",
      padding: 24,
    },
    button: {
      marginTop: 16,
      marginBottom: 16,
      paddingBottom: 12,
      paddingTop: 12,
    },
  })
);

export default useStyles;
