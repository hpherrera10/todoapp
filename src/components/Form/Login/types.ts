export interface State {
  userEmail: string;
  userPass: string;
  message: string;
}

export interface Props {
  handleSubmit: (user: any) => void;
}
