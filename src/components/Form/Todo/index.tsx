import { useState } from "react";
import useStyles from "./styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";

import { Props, State } from "./types";

function TodoForm(props: Props) {
  const initialState: State = {
    title: props?.title || "",
    description: props?.description || "",
  };

  const [state, setState] = useState(initialState);
  const styled = useStyles();

  return (
    <div>
      <TextField
        id="outlined-basic"
        label="Titulo"
        variant="outlined"
        margin="normal"
        fullWidth
        value={state.title}
        onChange={(e) => {
          setState({ ...state, title: e.target.value });
        }}
      />
      <TextField
        id="outlined-basic"
        label="Descripción"
        variant="outlined"
        fullWidth
        margin="normal"
        multiline
        rows={4}
        value={state.description}
        onChange={(e) => setState({ ...state, description: e.target.value })}
      />
      <div className={styled.buttonContainer}>
        <Button
          variant="contained"
          color="primary"
          startIcon={<SaveIcon />}
          onClick={() => props.onSave(state)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
}

export default TodoForm;
