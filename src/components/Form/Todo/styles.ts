import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonContainer: {
      width: "100%",
      textAlign: "right",
      marginTop: 16,
    },
  })
);

export default useStyles;
