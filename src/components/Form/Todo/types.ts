export interface State {
  title: string;
  description: string;
}

export interface Props {
  title?: string;
  description?: string;
  onSave: (todo: any) => void;
}
