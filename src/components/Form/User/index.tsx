import React, { useState } from "react";
import { Button, Card, Link, TextField, Typography } from "@material-ui/core";

import { Props, State } from "./types";
import useStyles from "./styles";
import { USerServ } from "../../../services";

function UserForm(props: Props) {
  const { handleSubmit } = props;

  const initialState: State = {
    userEmail: "",
    userPass: "",
    userName: "",
    confirmPass: "",
    message: "",
  };
  const [state, setState] = useState(initialState);

  const styled = useStyles();

  const handleRegister = () => {
    if (
      state.userName === "" ||
      state.userEmail === "" ||
      state.userPass === "" ||
      state.confirmPass === ""
    ) {
      setState({ ...state, message: "Todos los campos son requeridos" });
    }
    if (USerServ.validateEmail(state.userEmail)) {
      if (state.userPass === state.confirmPass) {
        handleSubmit(state);
      } else {
        setState({ ...state, message: "Las contraseñas no coinciden" });
      }
    } else {
      setState({
        ...state,
        message: "El campo email debe ser del tipo email",
      });
    }
  };

  const rendertitle = () => (
    <Typography variant="h5" component="h5" color="primary">
      Crear cuenta
    </Typography>
  );

  const renderName = () => (
    <TextField
      id="outlined-basic"
      label="Nombre"
      variant="outlined"
      margin="normal"
      fullWidth
      required
      value={state.userName}
      onChange={(e) => {
        setState({ ...state, userName: e.target.value, message: "" });
      }}
    />
  );

  const renderEmail = () => (
    <TextField
      id="outlined-basic"
      label="Email"
      variant="outlined"
      margin="normal"
      fullWidth
      type="email"
      required
      value={state.userEmail}
      onChange={(e) => {
        setState({ ...state, userEmail: e.target.value, message: "" });
      }}
    />
  );

  const renderPassword = () => (
    <TextField
      id="outlined-basic"
      label="Contraseña"
      variant="outlined"
      fullWidth
      type="password"
      margin="normal"
      required
      value={state.userPass}
      onChange={(e) =>
        setState({ ...state, userPass: e.target.value, message: "" })
      }
    />
  );

  const renderConfirmPassword = () => (
    <TextField
      id="outlined-basic"
      label="Contraseña"
      variant="outlined"
      fullWidth
      type="password"
      margin="normal"
      required
      value={state.confirmPass}
      onChange={(e) =>
        setState({ ...state, confirmPass: e.target.value, message: "" })
      }
    />
  );

  const renderMessage = () => (
    <Typography variant="body1" color="secondary">
      {state.message}
    </Typography>
  );

  const renderButton = () => (
    <Button
      variant="contained"
      color="primary"
      onClick={() => handleRegister()}
      fullWidth
      className={styled.button}
    >
      Registrarse
    </Button>
  );

  const renderLinkLogin = () => (
    <Typography>
      Si tienes una cuenta puedes{" "}
      <Link href="/login" variant="body2">
        iniciar sesión
      </Link>
    </Typography>
  );

  return (
    <Card className={styled.card}>
      {rendertitle()}
      {renderName()}
      {renderEmail()}
      {renderPassword()}
      {renderConfirmPassword()}
      {state.message !== "" && renderMessage()}
      {renderButton()}
      {renderLinkLogin()}
    </Card>
  );
}

export default UserForm;
