export interface State {
  userEmail: string;
  userPass: string;
  confirmPass: string;
  userName: string;
  message: string;
}

export interface Props {
  handleSubmit: (user: any) => void;
}
