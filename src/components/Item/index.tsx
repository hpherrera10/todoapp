import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Typography,
} from "@material-ui/core";
import { Edit, Delete, ExpandMore } from "@material-ui/icons";

import { Props } from "./types";
import useStyles from "./styles";

function TodoItem(props: Props) {
  const { todo, updateTodo, deleteTodo } = props;

  const styled = useStyles();

  const renderActions = () => (
    <Box className={styled.buttonAction}>
      <Button
        variant="contained"
        color="primary"
        startIcon={<Edit />}
        className={styled.button}
        onClick={() => updateTodo(todo)}
      >
        Editar
      </Button>
      <Button
        variant="contained"
        color="secondary"
        startIcon={<Delete />}
        className={styled.button}
        onClick={() => deleteTodo(todo)}
      >
        Eliminar
      </Button>
    </Box>
  );

  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMore />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography>{todo.title}</Typography>
      </AccordionSummary>
      <AccordionDetails className={styled.accordionDetails}>
        <Typography>{todo.description}</Typography>
        {renderActions()}
      </AccordionDetails>
    </Accordion>
  );
}

export default TodoItem;
