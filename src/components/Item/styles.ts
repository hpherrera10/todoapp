import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    accordionContainer: {
      marginRight: "25%",
      marginLeft: "25%",
      marginTop: 8,
    },
    accordionDetails: {
      backgroundColor: "#E7E7E7",
      flexDirection: "column",
      textAlign: "start",
    },
    buttonContainer: {
      marginRight: "25%",
      marginLeft: "25%",
      paddingTop: 16,
      textAlign: "end",
    },
    button: {
      margin: theme.spacing(1),
    },
    buttonAction: {
      textAlign: "right",
      marginTop: 64,
    },
  })
);

export default useStyles;
