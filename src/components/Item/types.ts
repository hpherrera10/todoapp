import { ITodo } from "../../screens/Todo/types";

export interface Props {
  todo: ITodo;
  updateTodo: (todo: any) => void;
  deleteTodo: (todo: any) => void;
}
