import { Box, Button, Modal, Typography } from "@material-ui/core";

import { Props } from "./types";
import useStyles from "./styles";

function ModalDelete(props: Props) {
  const { todoTitle, openModalDelete, onClose, handleDelete } = props;

  const styled = useStyles();

  const renderTitle = () => (
    <Typography variant="h5" className={styled.title}>
      Eliminar Tarea
    </Typography>
  );

  const renderMessage = () => (
    <Typography variant="body1" className={styled.title}>
      ¿Esta seguro que usted desea eliminar la tarea {todoTitle} ?
    </Typography>
  );

  const renderButtonAction = () => (
    <Box className={styled.buttonAction}>
      <Button
        variant="contained"
        color="default"
        className={styled.button}
        onClick={() => onClose()}
      >
        Cancelar
      </Button>
      <Button
        variant="contained"
        color="secondary"
        className={styled.button}
        onClick={() => handleDelete()}
      >
        Eliminar
      </Button>
    </Box>
  );

  return (
    <Modal
      open={openModalDelete}
      onClose={() => onClose()}
      className={styled.modal}
      disablePortal
      disableEnforceFocus
      disableAutoFocus
    >
      <Box className={styled.paper}>
        {renderTitle()}
        {renderMessage()}
        {renderButtonAction()}
      </Box>
    </Modal>
  );
}

export default ModalDelete;
