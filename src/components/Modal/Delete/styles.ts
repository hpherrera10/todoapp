import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: "flex",
      padding: theme.spacing(1),
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 4,
    },
    button: {
      margin: theme.spacing(1),
    },
    paper: {
      width: 400,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(2, 4, 3),
      borderRadius: 4,
    },
    title: {
      marginTop: 24,
      marginBottom: 24,
    },
    buttonAction: {
      textAlign: "right",
      marginTop: 64,
    },
    orderContainer: {
      marginRight: "25%",
      marginLeft: "25%",
      textAlign: "end",
      marginTop: 32,
    },
    buttonLogout: {
      textAlign: "right",
    },
  })
);

export default useStyles;
