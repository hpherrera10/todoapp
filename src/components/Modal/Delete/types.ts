export interface Props {
  todoTitle: string;
  openModalDelete: boolean;
  onClose: () => void;
  handleDelete: () => void;
}
