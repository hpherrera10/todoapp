import { Box, Modal, Typography } from "@material-ui/core";

import { Props } from "./types";
import useStyles from "./styles";
import TodoForm from "../../Form/Todo";

function ModalTodoForm(props: Props) {
  const { todo, openModal, isEdit, handleSubmit, handleUpdate, onClose } =
    props;

  const styled = useStyles();

  const rendertitle = () => (
    <Typography variant="h5" className={styled.title}>
      {isEdit ? "Editar Tarea" : "Crear Tarea"}
    </Typography>
  );

  const renderForm = () => (
    <TodoForm
      title={todo?.title}
      description={todo?.description}
      onSave={(todo: any) => (isEdit ? handleUpdate(todo) : handleSubmit(todo))}
    />
  );

  return (
    <Modal
      open={openModal}
      onClose={() => onClose()}
      className={styled.modal}
      disablePortal
      disableEnforceFocus
      disableAutoFocus
    >
      <Box className={styled.paper}>
        {rendertitle()}
        {renderForm()}
      </Box>
    </Modal>
  );
}

export default ModalTodoForm;
