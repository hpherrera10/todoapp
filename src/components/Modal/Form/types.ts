import { ITodo } from "../../../screens/Todo/types";

export interface Props {
  todo?: ITodo;
  openModal: boolean;
  isEdit?: boolean;
  handleSubmit: (user: any) => void;
  handleUpdate: (user: any) => void;
  onClose: () => void;
}
