import { StateProps, DispatchProps } from "../../screens/Login/types";
import { connect } from "react-redux";

import Login from "../../screens/Login/Screen";
import { loginUser } from "../../store/login/actions";

function mapStateToProps(state: any): StateProps {
  return {
    user: state.login.user,
  };
}

function mapDispatchToProps(dispatch: any): DispatchProps {
  return {
    loginUser(email: string, password: string) {
      dispatch(loginUser(email, password));
    },
  };
}

const ConnectedComponent = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default ConnectedComponent;
