import { StateProps, DispatchProps } from "../../screens/SignUp/types";
import { connect } from "react-redux";

import SignUp from "../../screens/SignUp/Screen";
import { registerUser } from "../../store/user/actions";

function mapStateToProps(state: any): StateProps {
  return {
    user: state.user.user,
  };
}

function mapDispatchToProps(dispatch: any): DispatchProps {
  return {
    registerUser(user: any) {
      dispatch(registerUser(user));
    },
  };
}

const ConnectedComponent = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);

export default ConnectedComponent;
