import { StateProps, DispatchProps, ITodo } from "../../screens/Todo/types";
import { connect } from "react-redux";

import Todo from "../../screens/Todo/Screen";
import {
  getTodos,
  addTodo,
  putTodo,
  deleteTodo,
} from "../../store/todo/actions";
import { logout } from "../../store/user/actions";

function mapStateToProps(state: any): StateProps {
  console.log(state);
  return {
    todoList: state.todo.todos,
    actionMessage: state.todo.message,
    loading: state.remote.loading.Todo,
    logout: state.user.logout,
  };
}

function mapDispatchToProps(dispatch: any): DispatchProps {
  return {
    loadTodos(params: any) {
      dispatch(getTodos(params));
    },
    addTodo(todo: any) {
      dispatch(addTodo(todo));
    },
    updateTodo(todo: ITodo, todoId: number) {
      dispatch(putTodo(todo, todoId));
    },
    deleteTodo(todoId: number) {
      dispatch(deleteTodo(todoId));
    },
    userLogout() {
      dispatch(logout());
    },
  };
}

const ConnectedComponent = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(Todo);

export default ConnectedComponent;
