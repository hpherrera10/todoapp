import { useEffect } from "react";
import { Grid } from "@material-ui/core";
import { useNavigate } from "react-router-dom";

import { Props } from "./types";
import useStyles from "./styles";
import LoginForm from "../../components/Form/Login";

function Login(props: Props) {
  const { user, loginUser } = props;
  const styled = useStyles();
  let navigate = useNavigate();

  useEffect(() => {
    if (user && user.logged_in) {
      navigate("/todo");
    }
  }, [user]);

  return (
    <Grid container justify="center" className={styled.container}>
      <LoginForm
        handleSubmit={(user: any) => loginUser(user.userEmail, user.userPass)}
      />
    </Grid>
  );
}

export default Login;
