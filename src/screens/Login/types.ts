export interface State {
  userEmail: string;
  userPass: string;
}

export interface StateProps {
  user: any;
}

export interface DispatchProps {
  loginUser: (email: string, password: string) => void;
}

export interface Props extends StateProps, DispatchProps {}
