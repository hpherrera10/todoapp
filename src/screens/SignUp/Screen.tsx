import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Grid } from "@material-ui/core";

import { Props } from "./types";
import useStyles from "./styles";
import UserForm from "../../components/Form/User";
import Alert from "../../components/Alert";

function SignUP(props: Props) {
  const { user, registerUser } = props;
  const navigate = useNavigate();
  const styled = useStyles();

  useEffect(() => {
    if (user && user.created) {
      navigate("/", {
        state: {
          created: user.created,
        },
      });
    }
  }, [user]);

  const renderAlert = () => <Alert message={user.error} />;

  const renderForm = () => (
    <UserForm handleSubmit={(user) => registerUser(user)} />
  );

  return (
    <Grid container justify="center" className={styled.container}>
      {user && user.error && renderAlert()}
      {renderForm()}
    </Grid>
  );
}

export default SignUP;
