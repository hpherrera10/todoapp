export interface State {
  userName: string;
  userEmail: string;
  userPass: string;
  userConfirmPass: string;
}

export interface StateProps {
  user: any;
}

export interface DispatchProps {
  registerUser: (user: any) => void;
}

export interface Props extends StateProps, DispatchProps {}
