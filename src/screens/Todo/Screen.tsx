import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Container,
  Typography,
  IconButton,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { Add, ArrowUpward, ArrowDownward, ExitToApp } from "@material-ui/icons";

import { Props, ITodo, State } from "./types";
import useStyles from "./styles";
import ModalDelete from "../../components/Modal/Delete";
import TodoItem from "../../components/Item";
import ModalTodoForm from "../../components/Modal/Form";
import Alert from "../../components/Alert";

function Todo(props: Props) {
  const {
    actionMessage,
    todoList,
    loading,
    logout,
    addTodo,
    loadTodos,
    updateTodo,
    deleteTodo,
    userLogout,
  } = props;

  const initialState: State = {
    openModal: false,
    openModalDelete: false,
    openAlert: true,
    isEdit: false,
    todo: { id: null, title: "", description: "" },
  };
  const [state, setState] = useState(initialState);

  const styled = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    getTodos({});
  }, [actionMessage]);

  useEffect(() => {
    console.log("todo", logout);
    if (logout && logout.logged_out) {
      navigate("/");
    }
  }, [logout]);

  const getTodos = (order: any) => {
    loadTodos(order);
  };

  const renderListItem = () => {
    let view = <Box />;

    if (todoList.length) {
      view = renderItem();
    }

    return view;
  };

  const renderItem = () => (
    <Container maxWidth="md">
      {todoList &&
        todoList.map((todo: ITodo, index: number) => (
          <TodoItem
            key={index}
            todo={todo}
            updateTodo={() =>
              setState({
                ...state,
                isEdit: true,
                todo: todo,
                openModal: true,
              })
            }
            deleteTodo={() =>
              setState({ ...state, todo: todo, openModalDelete: true })
            }
          />
        ))}
    </Container>
  );

  const renderOrderActions = () => (
    <Box className={styled.orderContainer}>
      <IconButton onClick={() => getTodos("LOWER(title) DESC")}>
        <ArrowUpward />
      </IconButton>
      <IconButton onClick={() => getTodos("LOWER(title) ASC")}>
        <ArrowDownward />
      </IconButton>
      <Typography variant="body1">Ordenar Por Nombre</Typography>
    </Box>
  );

  const renderModalDelete = () => (
    <ModalDelete
      todoTitle={state.todo.title}
      openModalDelete={state.openModalDelete}
      onClose={() => setState({ ...state, openModalDelete: false })}
      handleDelete={() => {
        deleteTodo(state.todo.id);
        setState({ ...state, openModalDelete: false });
      }}
    />
  );

  const renderLogout = () => (
    <Box className={styled.buttonLogout}>
      <Button
        variant="contained"
        color="primary"
        startIcon={<ExitToApp />}
        onClick={() => userLogout()}
      >
        Cerrar sesión
      </Button>
    </Box>
  );

  const renderActionAddTodo = () => (
    <Box className={styled.buttonContainer}>
      <Button
        variant="contained"
        color="default"
        startIcon={<Add />}
        onClick={() => {
          setState({
            ...state,
            isEdit: false,
            openModal: true,
            todo: { id: null, title: "", description: "" },
          });
        }}
      >
        Agregar Tarea
      </Button>
    </Box>
  );

  const rendertitle = () => (
    <Typography variant="h2" className={styled.heading}>
      Lista de tareas
    </Typography>
  );

  const renderModalForm = () => (
    <ModalTodoForm
      todo={state.todo}
      isEdit={state.isEdit}
      openModal={state.openModal}
      onClose={() => setState({ ...state, openModal: !state.openModal })}
      handleSubmit={(todo) => {
        addTodo(todo);
        setState({
          ...state,
          openModal: !state.openModal,
          isEdit: false,
          todo: { id: null, title: "", description: "" },
        });
        getTodos({});
      }}
      handleUpdate={(todo) => {
        updateTodo(todo, state.todo.id);
        setState({
          ...state,
          openModal: !state.openModal,
          isEdit: false,
          todo: { id: null, title: "", description: "" },
        });
      }}
    />
  );

  const renderAlert = () => <Alert message={actionMessage} />;

  const renderData = () => {
    let view = <CircularProgress />;

    if (!loading) {
      view = (
        <>
          {renderLogout()}
          {rendertitle()}
          {renderAlert()}
          {renderOrderActions()}
          {renderListItem()}
          {renderActionAddTodo()}
          {renderModalForm()}
          {renderModalDelete()}
        </>
      );
    }

    return view;
  };

  return <Box className={styled.root}>{renderData()}</Box>;
}

export default Todo;
