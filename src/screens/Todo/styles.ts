import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      alignItems: "center",
      textAlign: "center",
    },
    heading: {
      marginTop: 32,
    },
    accordionContainer: {
      marginRight: "30%",
      marginLeft: "25%",
      marginTop: 8,
    },
    accordionDetails: {
      backgroundColor: "#E7E7E7",
      flexDirection: "column",
      textAlign: "start",
    },
    buttonContainer: {
      marginRight: "18%",
      paddingTop: 24,
      textAlign: "end",
      marginTop: 32,
    },
    button: {
      margin: theme.spacing(1),
    },
    modal: {
      display: "flex",
      padding: theme.spacing(1),
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 4,
    },
    paper: {
      width: 400,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(2, 4, 3),
      borderRadius: 4,
    },
    title: {
      marginTop: 24,
      marginBottom: 24,
    },
    buttonAction: {
      textAlign: "right",
      marginTop: 64,
    },
    orderContainer: {
      marginRight: "18%",
      textAlign: "end",
      justifyContent: "end",
      alignItems: "center",
      marginTop: 32,
      marginBottom: 32,
      display: "flex",
      flexDirection: "row",
    },
    buttonLogout: {
      textAlign: "right",
    },
  })
);

export default useStyles;
