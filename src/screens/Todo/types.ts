export interface State {
  openModal: boolean;
  openModalDelete: boolean;
  openAlert: boolean;
  isEdit: boolean;
  todo: ITodo;
}

export interface ITodo {
  id: any;
  title: string;
  description: string;
}

export interface StateProps {
  todoList: ITodo[];
  actionMessage: string;
  loading: boolean;
  logout: any;
}

export interface DispatchProps {
  loadTodos: (params: any) => void;
  addTodo: (todo: any) => void;
  updateTodo: (todo: ITodo, todoId: number) => void;
  deleteTodo: (id: number) => void;
  userLogout: () => void;
}

export interface Props extends StateProps, DispatchProps {}
