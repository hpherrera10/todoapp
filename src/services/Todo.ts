import axios from "axios";
import { ITodo } from "../screens/Todo/types";

const API_URL = "http://localhost:5000/api/v1/todos";

function getTodos(order: any) {
  return axios
    .get(API_URL, {
      params: { sort_by: order },
    })
    .then((response) => {
      if (response) {
        return response.data;
      }
      return null;
    });
}

function addTodo(todo: ITodo) {
  return axios.post(API_URL, todo).then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
}

function putTodo(todoId: number, todo: ITodo) {
  return axios.put(API_URL + "/" + todoId, todo).then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
}

function deleteTodo(todoId: number) {
  return axios.delete(API_URL + "/" + todoId).then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
}

export default {
  getTodos,
  addTodo,
  putTodo,
  deleteTodo,
};
