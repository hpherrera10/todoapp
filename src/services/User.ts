import axios from "axios";

const API_URL = "http://localhost:5000/api/v1/";

const register = (user: any) => {
  return axios
    .post("http://localhost:5000/api/v1/users", {
      user: {
        name: user.userName,
        email: user.userEmail,
        password: user.userPass,
        password_confirmation: user.userConfirmPass,
      },
    })
    .then((response) => {
      if (response) {
        return response.data;
      }
      return null;
    });
};

const login = (email: string, password: string) => {
  return axios
    .post(API_URL + "login", {
      email,
      password,
    })
    .then((response) => {
      if (response) {
        return response.data;
      }
      return null;
    });
};

const logout = () => {
  console.log("sii");
  return axios.delete(API_URL + "logout").then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
};

function validateEmail(email: string) {
  let emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let isValidEmail = email && email.length > 0 && emailRegex.test(email);
  return isValidEmail;
}

export default {
  register,
  login,
  logout,
  validateEmail,
};
