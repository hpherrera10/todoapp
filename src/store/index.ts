import { configureStore } from "@reduxjs/toolkit";

import { userReducer } from "./user";
import { todoReducer } from "./todo";
import { remoteReducer } from "./remote";
import { loginReducer } from "./login";

const reducer = {
  user: userReducer,
  todo: todoReducer,
  remote: remoteReducer,
  login: loginReducer,
};

const store = configureStore<any, any>({
  reducer,
});

export default store;
