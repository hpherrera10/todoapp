import { USerServ } from "../../services";
import { logoutSuccess } from "../user/actions";
import { remoteCall, createAction } from "../utils";

export const loginSuccess = createAction("login/loginSuccess");
export const loginError = createAction("login/loginError");

export const loginUser = (email: string, password: string) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Login", () => USerServ.login(email, password))
      .then((user: any) => {
        dispatch(loginSuccess(user));
        dispatch(logoutSuccess(null));
      })
      .catch((error) => dispatch(loginError(error.message)));
};
