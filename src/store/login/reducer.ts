import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import { loginSuccess, loginError } from "./actions";

export interface State {
  isLoggedIn: boolean;
  user: any;
  error: string;
}

const initialState: State = {
  isLoggedIn: false,
  user: null,
  error: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [loginSuccess.type]: (state, action) => {
    state.user = action.payload;
  },
  [loginError.type]: (state, action) => {
    state.error = action.payload;
  },
});

export default reducer;
