import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import { loading } from "./actions";

export interface State {
  loading: { [key: string]: boolean };
}

const initialState: State = {
  loading: {},
};

const reducer: Reducer<State> = createReducer(initialState, {
  [loading.type]: (state, action) => {
    state.loading[action.payload.context] = action.payload.loading === true;
  },
});

export default reducer;
