import { createAction, remoteCall } from "../utils";
import { TodoServ } from "../../services";
import { ITodo } from "../../screens/Todo/types";

export const getTodosSuccess = createAction("todo/getTodosSuccess");
export const getTodosError = createAction("todo/getTodosError");
export const addTodoSuccess = createAction("todo/addTodoSuccess");
export const addTodoError = createAction("todo/addTodoError");
export const putTodoSuccess = createAction("todo/putTodoSuccess");
export const putTodoError = createAction("todo/putTodoError");
export const deleteTodoSuccess = createAction("todo/deleteTodoSuccess");
export const deleteTodoError = createAction("todo/deleteTodoError");

export const getTodos = (params: any) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Todo", () => TodoServ.getTodos(params))
      .then((todos: any) => dispatch(getTodosSuccess(todos)))
      .catch((error) => dispatch(getTodosError(error.message)));
};

export const addTodo = (todo: ITodo) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Todo", () => TodoServ.addTodo(todo))
      .then((response: string) => dispatch(addTodoSuccess(response)))
      .catch((error) => dispatch(addTodoError(error.message)));
};

export const putTodo = (todo: ITodo, todoId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Todo", () => TodoServ.putTodo(todoId, todo))
      .then((response: string) => dispatch(putTodoSuccess(response)))
      .catch((error) => dispatch(putTodoError(error.message)));
};

export const deleteTodo = (todoId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Todo", () => TodoServ.deleteTodo(todoId))
      .then((response: string) => dispatch(deleteTodoSuccess(response)))
      .catch((error) => dispatch(deleteTodoError(error.message)));
};
