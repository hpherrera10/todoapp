import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import {
  getTodosSuccess,
  getTodosError,
  addTodoSuccess,
  addTodoError,
  putTodoSuccess,
  putTodoError,
  deleteTodoSuccess,
  deleteTodoError,
} from "./actions";

export interface State {
  todos: any;
  error: string;
  message: string;
}

const initialState: State = {
  todos: [],
  error: "",
  message: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [getTodosSuccess.type]: (state, action) => {
    state.todos = action.payload;
  },
  [getTodosError.type]: (state, action) => {
    state.error = action.payload;
  },
  [addTodoSuccess.type]: (state, action) => {
    const { title } = action.payload;
    state.message = `Tarea ${title} agregada`;
  },
  [addTodoError.type]: (state) => {
    state.message = "Error para agregar tarea";
  },
  [putTodoSuccess.type]: (state, action) => {
    const { title } = action.payload;
    state.message = `Tarea ${title} actualizada`;
  },
  [putTodoError.type]: (state) => {
    state.message = "La tarea no se pudo actualizar";
  },
  [deleteTodoSuccess.type]: (state, action) => {
    const { todo_title } = action.payload;
    state.message = `Tarea ${todo_title} eliminada`;
  },
  [deleteTodoError.type]: (state) => {
    state.message = "La tarea no se pudo eliminar";
  },
});

export default reducer;
