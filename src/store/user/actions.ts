import { USerServ } from "../../services";
import { loginSuccess } from "../login/actions";
import { remoteCall, createAction } from "../utils";

export const registerSuccess = createAction("user/registerSuccess");
export const registerError = createAction("user/registerError");
export const logoutSuccess = createAction("user/logoutSuccess");
export const logoutError = createAction("user/logoutError");

export const registerUser = (user: any) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "User", () => USerServ.register(user))
      .then((user: any) => dispatch(registerSuccess(user)))
      .catch((error) => dispatch(registerError(error.message)));
};

export const logout = () => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Login", () => USerServ.logout())
      .then((response: any) => {
        dispatch(logoutSuccess(response));
        dispatch(loginSuccess(null));
      })
      .catch((error) => dispatch(logoutError(error.message)));
};
