import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import {
  registerSuccess,
  registerError,
  logoutSuccess,
  logoutError,
} from "./actions";

export interface State {
  user: any;
  error: string;
  logout: any;
}

const initialState: State = {
  user: null,
  error: "",
  logout: null,
};

const reducer: Reducer<State> = createReducer(initialState, {
  [registerSuccess.type]: (state, action) => {
    state.user = action.payload;
  },
  [registerError.type]: (state, action) => {
    state.error = action.payload;
  },
  [logoutSuccess.type]: (state, action) => {
    state.logout = action.payload;
  },
  [logoutError.type]: (state, action) => {
    state.error = action.payload;
  },
});

export default reducer;
